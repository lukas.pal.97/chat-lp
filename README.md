# ChatApp

by Łukasz Pałys

## How to run

### Backend

Go to `chat-server` and run `npm install`

Go to `chat-server/bin` and run `node www`

### Frontend

Go to `chat-app` and run `npm install`

Run `ng serve`. Navigate to `http://localhost:4200/`.

## Chat app screens

![Login page of chatapp](login-example.PNG "Login page")

![Chat page of chatapp](chat-example.PNG "Chat page")



