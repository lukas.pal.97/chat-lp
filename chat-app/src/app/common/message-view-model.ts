
export class MessageViewModel{
  constructor(
  public username: string,
  public message: string,
  public isInfo: boolean = false,
  public isMyMsg: boolean = false,
  ) {
  }
}
