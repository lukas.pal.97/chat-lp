
export class UserAmountChangedViewModel{
  public username: string;
  public numUsers: number;
}

export class UserViewModel{
  public username: string;
}

export class UserAmountViewModel{
  public numUsers: number;
}
