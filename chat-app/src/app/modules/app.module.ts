import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from '../components/app.component';
import { LoginPageComponent } from '../components/login-page.component';
import { ChatPageComponent } from '../components/chat-page.component';
import {FormsModule} from "@angular/forms";
import {SocketIoConfig, SocketIoModule} from "ngx-socket-io";
import {environment} from "../../environments/environment";

const config: SocketIoConfig = {
	url: environment.socketUrl,
	options: {
		transports: ['websocket']
	}
};

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    ChatPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    SocketIoModule.forRoot(config),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
