import { Injectable } from '@angular/core';
import {Socket} from "ngx-socket-io";
import {Observable} from "rxjs";
import {MessageViewModel} from "../common/message-view-model";
import {UserAmountChangedViewModel, UserAmountViewModel, UserViewModel} from "../common/socket-result-view-models";

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  constructor(private socket: Socket) {
  }

  setUserName(name: string): void {
    this.socket.emit('add user', name);
  }

  sendMessage(msg: string): void{
    this.socket.emit('new message', msg);
  }

  startTyping(): void{
    this.socket.emit('typing');
  }

  stopTyping(): void{
    this.socket.emit('stop typing');
  }

  onGetNewMessage(): Observable<MessageViewModel> {
    return this.socket.fromEvent<MessageViewModel>('new message');
  }

  onUserJoined(): Observable<UserAmountChangedViewModel>{
    return  this.socket.fromEvent<UserAmountChangedViewModel>('user joined');
  }

  onUserLeft(): Observable<UserAmountChangedViewModel>{
    return this.socket.fromEvent<UserAmountChangedViewModel>('user left');
  }

  onTyping(): Observable<UserViewModel>{
    return this.socket.fromEvent<UserViewModel>('typing');
  }

  onStopTyping(): Observable<UserViewModel>{
    return  this.socket.fromEvent<UserViewModel>('stop typing');
  }

  onLogin(): Observable<UserAmountViewModel>{
    return this.socket.fromEvent<UserAmountViewModel>('login');
  }
}
