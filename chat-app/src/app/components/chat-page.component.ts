import {AfterViewChecked, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {MessageViewModel} from "../common/message-view-model";
import {SocketService} from "../services/socket.service";
import { map, tap} from "rxjs/operators";
import Timeout = NodeJS.Timeout;

@Component({
  selector: 'app-chat-page',
  templateUrl: './chat-page.component.html',
  styleUrls: ['./chat-page.component.css']
})
export class ChatPageComponent implements OnInit, AfterViewChecked {

  @Input() userName: string;
  @Input() numUsers: number;
  @ViewChild('messageInput') messageInput: ElementRef;
  @ViewChild('messagesList') messagesList: ElementRef;
  message: string;
  messages: MessageViewModel[] = [];
  currentlyTyping: string[] = [];
  isTyping: boolean = false;
  timeoutId: Timeout;

  constructor(
    private socketService: SocketService,
  ) { }

  ngOnInit(): void {
    this.handleEvents();
  }

  ngAfterViewChecked(): void {
    this.scrollToBottom();
  }

  private handleEvents(): void{
    this.socketService.onGetNewMessage().subscribe(data => this.messages.push(data));

    this.socketService.onUserJoined().pipe(
      tap(data => this.numUsers = data.numUsers),
      map(data => {
      return new MessageViewModel('', `${data.username} joined\nthere are ${data.numUsers} participants`, true);
    })).subscribe(data => this.messages.push(data));

    this.socketService.onUserLeft().pipe(
      tap(data => this.numUsers = data.numUsers),
      map(data => {
      return new MessageViewModel('', `${data.username} left\nthere are ${data.numUsers} participants`, true);
    })).subscribe(data => this.messages.push(data));

    this.socketService.onTyping().subscribe(data => this.currentlyTyping.push(data.username));

    this.socketService.onStopTyping().subscribe(data => {
      this.currentlyTyping = this.currentlyTyping.filter(x => x !== data.username);
    })
  }

  private stopTyping(): void{
    this.socketService.stopTyping();
  }

  private startTyping(): void{
    this.socketService.startTyping();
  }

  private scrollToBottom(): void{
    try{
      this.messagesList.nativeElement.scrollTop = this.messagesList.nativeElement.scrollHeight;
    } catch (err) {

    }
  }

  keyup(): void{
    if(this.isTyping){
      clearTimeout(this.timeoutId);
    }
    else{
      this.isTyping = true;
      this.startTyping();
    }
    this.timeoutId = setTimeout(() => {
      if(this.isTyping)
      {
        this.stopTyping();
        this.isTyping = false;
      }
    }, 1000);
  }

  send(): void{
    if(this.message != undefined && this.message.length > 0){
      this.socketService.sendMessage(this.message);
      let msgViewModel = new MessageViewModel(this.userName, this.message, false, true);
      this.messages.push(msgViewModel);
      this.message = ''
    }
  }

}
