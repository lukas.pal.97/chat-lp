import { Component } from '@angular/core';
import {UserLoggedViewModel} from "../common/user-logged-view-model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  logged: boolean = false;
  userName: string;
  numUsers: number;

  login(data: UserLoggedViewModel): void{
    this.logged = true;
    this.userName = data.userName;
    this.numUsers = data.numUsers;
  }

}
