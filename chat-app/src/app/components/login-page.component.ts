import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SocketService} from "../services/socket.service";
import {UserLoggedViewModel} from "../common/user-logged-view-model";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit{

  userName: string;

  @Output() loginEvent = new EventEmitter<UserLoggedViewModel>();

  constructor(
    private socketService: SocketService,
  ) { }

  ngOnInit(): void {
    this.socketService.onLogin().pipe(
      map(data => new UserLoggedViewModel(this.userName, data.numUsers))
    ).subscribe(data => this.loginEvent.emit(data));
  }

  login(): void {
    if(this.userName != undefined && this.userName.length > 0)
      this.socketService.setUserName(this.userName);
  }
}
