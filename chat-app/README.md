# ChatApp

by Łukasz Pałys

## How to run

Run `node chat-server/bin/www` to start backend server.

Run `ng serve` in chat-app to serve frontend app. Navigate to `http://localhost:4200/`.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Chat app screens

![Login page of chatapp](login-example.PNG "Login page")

![Chat page of chatapp](chat-example.PNG "Chat page")



